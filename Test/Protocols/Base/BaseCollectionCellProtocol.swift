//
//  BaseCollectionCellProtocol.swift
//  Test
//
//  Created by vaskov on 07.12.2019.
//  Copyright © 2019 vaskov. All rights reserved.
//

import UIKit


protocol BaseCollectionCellProtocol {
    associatedtype Model
    static var identifier: String { get }
    
    func update(_ model: Model)
}

//class BaseCollectionCell: UICollectionViewCell, CollectionCellProtocol {
//    static var identifier = "I"
//
//    static func size(_ model: Any) -> CGSize {
//        return CGSize()
//    }
//
//    func update(_ model: Any) {}
//}
