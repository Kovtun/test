//
//  UIView+ConstraintsExtension.swift
//  Test
//
//  Created by vaskov on 17.08.17.
//  Copyright © 2017 Home. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    public func constraint(_ identifier: String) -> NSLayoutConstraint? {
        return constraint(identifier, view: self)
    }

    public func constraints(_ identifiers: [String]) -> [NSLayoutConstraint] {
        var constraints: [NSLayoutConstraint] = []

        for identifier in identifiers {
            let constraintSubview = constraint(identifier)
            if constraintSubview != nil {
                constraints.append(constraintSubview!)
            }
        }

        return constraints
    }

    public func constraint(_ identifier: String, view: UIView) -> NSLayoutConstraint? {
        for constraint in view.constraints {
            if identifier == constraint.identifier {
                return constraint
            }
        }

        for subview in view.subviews {
            let constraintSubview = constraint(identifier, view: subview)
            guard constraintSubview == nil else {
                return constraintSubview
            }
        }

        return nil
    }
}


