//
//  UIViewController+Extensions.swift
//  Cosmunity
//
//  Created by vaskov on 3/15/17.
//  Copyright © 2017 Home Inc. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    func presentViewController(_ viewController: UIViewController) {
        if let navController = self.navigationController {
            navController.pushViewController(viewController, animated: true)
            return
        }
        self.present(viewController, animated: true, completion: nil)
    }
    
    class func topViewController(_ controller: UIViewController? =  UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
        guard let control = controller else {
            return nil
        }
        
        if let navigationController = control as? UINavigationController {
            return topViewController(navigationController.visibleViewController)
        }
        
        if let tabController = control as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presented = control.presentedViewController {
            return topViewController(presented)
        }
        
        if control is UIAlertController && AlertHelper.staticController != nil {
            return AlertHelper.staticController!
        }
        
        return control
    }
    
    class func presentViewController(_ viewController: UIViewController, animated: Bool? = true) {
        guard let controller = self.topViewController() else {
            return
        }
        if let navController = controller as? UINavigationController {
            return navController.pushViewController(viewController, animated: animated!)
        } else if let navController = controller.navigationController {
            return navController.pushViewController(viewController, animated: animated!)
        }
        return controller.present(viewController, animated: animated!, completion: nil)
    }
}
