//
//  UIImage+Extensions.swift
//  Table
//
//  Created by vaskov on 04.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit


extension UIImage {
    func newImage(newSize: CGSize) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image { _ in
            self.draw(in: CGRect.init(origin: CGPoint.zero, size: newSize))
        }

        return image
    }
    
    func newImage(newSize: CGSize, isProportional: Bool) -> UIImage {
        guard isProportional else {
            return newImage(newSize: newSize)
        }
        
        let scaleWidth = newSize.width / self.size.width
        let scaleHeight = newSize.height / self.size.height
        
        let scale = scaleWidth > scaleHeight ? scaleWidth : scaleHeight
        let size = CGSize(width: self.size.width * scale, height: self.size.height * scale)
        
        return newImage(newSize: size)
    }
}
