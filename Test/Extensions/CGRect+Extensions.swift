//
//  CGRect+Extensions.swift
//  Test
//
//  Created by vaskov on 09.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit


extension CGRect {
    var top: CGFloat {
        set {
            let or = CGPoint(x: self.origin.x, y: newValue)
            self.origin = or
        }
        get {
            return self.origin.y
        }
    }
    
    var height: CGFloat {
        set {
            let s = CGSize(width: self.size.width, height: newValue)
            self.size = s
        }
        get {
            return self.origin.y
        }
    }
}
