//
//  UIColor+Extensions.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit


extension UIColor {
    class func randomColor() -> UIColor {
        func r() -> CGFloat {
            return CGFloat(Float.random(in: 0..<1))
        }
        return UIColor(red: r(), green: r(), blue: r(), alpha: 1)
    }
}
