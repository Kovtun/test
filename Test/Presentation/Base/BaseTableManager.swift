//
//  TableManager.swift
//  Test
//
//  Created by vaskov on 21.06.2018.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


//lazy var tableManager: TTTableManager = {
//    let manager = TTTableManager()
//    manager.configManager(tableView, delegate: self)
//    return manager
//}()


class BaseTableManager<Cell: BaseTableCellProtocol, Model: Any>: NSObject, UITableViewDataSource, UITableViewDelegate where Cell.Model == Model {
    weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    
    var items: [Model] = []
    
    var isShowRefreshControl = false {
        didSet {
            if isShowRefreshControl {
                self.configRefreshControl()
            } else {
                refreshControl.removeFromSuperview()
            }
        }
    }
    
    func configManager(_ tableView: UITableView) {
        self.tableView = tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44.0
        
        tableView.register(UINib(nibName: Cell.identifier, bundle: nil), forCellReuseIdentifier: Cell.identifier)
    }
    
    // MARK: - UITableViewDataSource -
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = items[indexPath.row] as Model
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier)
        (cell as! Cell).update(model)
        fillCell((cell as! Cell), model: model, indexPath: indexPath)
        
        return cell!
    }
    
    // MARK: - UITableViewDelegate -
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = items[indexPath.row]
        return Cell.height(model)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func fillCell(_ cell: Cell, model: Model, indexPath: IndexPath) {}
    
    // MARK: - UIScrollViewDelegate -
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        refreshControl.alpha = scrollView.contentOffset.y / -refreshControl.height
        tableView.invalidateIntrinsicContentSize()
    }
}

// MARK: - Privates
extension BaseTableManager {
    fileprivate func configRefreshControl() {
        refreshControl.addTarget(tableView, action: #selector(UITableView.reloadData), for: .valueChanged)
        tableView.insertSubview(refreshControl, at: 0)
    }
}
