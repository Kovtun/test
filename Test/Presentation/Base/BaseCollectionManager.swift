//
//  BaseCollectionManager.swift
//  CrowdThinc
//
//  Created by vaskov on 21.06.2018.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


//lazy var collectionManager: TTCollectionManager = {
//    let manager = TTCollectionManager()
//    manager.configManager(collectionView, delegate: self)
//    return manager
//}()

class BaseCollectionManager<Cell: BaseCollectionCellProtocol, Model: Mappable>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout where Cell.Model == Model {
    weak var collectionView: UICollectionView?
    
    var items: [Model] = [] {
        didSet {
            self.setDefaults()
        }
    }

    func configManager(_ collectionView: UICollectionView) {
        self.collectionView = collectionView
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.identifier, for: indexPath)
        (cell as! Cell).update(items[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {}
}

// MARK: - Privates
extension BaseCollectionManager {
    fileprivate func setDefaults() {
        self.collectionView?.register(UINib(nibName: Cell.identifier, bundle: nil), forCellWithReuseIdentifier: Cell.identifier)
    }
}
