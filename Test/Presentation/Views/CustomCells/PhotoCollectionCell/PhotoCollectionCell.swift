//  
//  PhotoCollectionCell.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class PhotoCollectionCell: UICollectionViewCell, BaseCollectionCellProtocol, CashImagesProtocol {
    typealias Model = Photo
    static let identifier: String = "PhotoCollectionCell"
    
    @IBOutlet var iconImageView: UIImageView?
    @IBOutlet var activityIndicator: UIActivityIndicatorView?
    
    var model: Photo!
    var urlStr: String!
    
    private var imageObs: Observable<UIImage??>!
    private var disposeBag = DisposeBag()
    
    func update(_ model: Photo) {
        self.model = model
        
        if let url = model.urls?.thumb, urlStr != url {
            iconImageView?.image = nil
            urlStr = url
            setImage(url: url, imageView: iconImageView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageObs = iconImageView!.rx.observe(Optional<UIImage>.self, "image")
        imageObs.map { [weak self] (image) in
            if image == nil {
                self?.activityIndicator?.isHidden = false
                self?.activityIndicator?.startAnimating()
            } else {
                self?.activityIndicator?.isHidden = true
                self?.activityIndicator?.stopAnimating()
            }
        }.subscribe().disposed(by: disposeBag)
    }
}
