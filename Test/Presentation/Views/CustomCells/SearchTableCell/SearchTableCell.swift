//  
//  SearchTableCell.swift
//  Test
//
//  Created by vaskov on 09.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit
import MBProgressHUD
import RxCocoa
import RxSwift

protocol SearchTableCellDelegate: class {
    func cell(remove cell: SearchTableCell)
}


class SearchTableCell: UITableViewCell, BaseTableCellProtocol, CashImagesProtocol {
    typealias Model = Photo
    static let identifier: String = "SearchTableCell"
    
    @IBOutlet var iconImageView: UIImageView?
    @IBOutlet var activityIndicator: UIActivityIndicatorView?
    @IBOutlet weak var removeButton: UIButton!
    
    var model: Photo!
    var urlStr: String!
    weak var delegate: SearchTableCellDelegate?
    
    private var imageObs: Observable<UIImage??>!
    private var disposeBag = DisposeBag()
    
    static func height(_ model: Model) -> CGFloat {
        return 300
    }
    
    func update(_ model: Photo) {
        self.model = model
        
        if let url = model.urls?.regular, urlStr != url {
            iconImageView?.image = nil
            urlStr = url
            setImage(url: url, imageView: iconImageView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageObs = iconImageView!.rx.observe(Optional<UIImage>.self, "image")
        imageObs.map { [weak self] (image) in
            if image == nil {
                self?.activityIndicator?.isHidden = false
                self?.activityIndicator?.startAnimating()
            } else {
                self?.activityIndicator?.isHidden = true
                self?.activityIndicator?.stopAnimating()
            }
        }.subscribe().disposed(by: disposeBag)
    }
    
    @IBAction func remove() {
        delegate?.cell(remove: self)
    }
}
