//  
//  PhotoCollectionManager.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit

protocol PhotoCollectionManagerDelegate: class {
    func manager(_ manager: PhotoCollectionManager, didSelectItem indexPath: IndexPath)
}

class PhotoCollectionManager: BaseCollectionManager<PhotoCollectionCell, Photo> {
    weak var delegate: PhotoCollectionManagerDelegate?

    // MARK: - Publics -
    func configManager(_ collectionView: UICollectionView, delegate: PhotoCollectionManagerDelegate) {
        self.delegate = delegate
        
        let width = CGFloat((SCREEN_WIDTH - 40) / CGFloat(Constants.numberRows))
        let size = CGSize(width: width, height: width * 1.5)
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.estimatedItemSize = size
        flow.itemSize = size
        flow.minimumLineSpacing = 5
        flow.minimumInteritemSpacing = 5
        collectionView.collectionViewLayout = flow
        super.configManager(collectionView)
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        delegate?.manager(self, didSelectItem: indexPath)
    }
}
