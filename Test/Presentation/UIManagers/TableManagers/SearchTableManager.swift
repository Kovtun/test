//  
//  SearchTableManager.swift
//  Test
//
//  Created by vaskov on 09.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit

protocol SearchTableManagerDelegate: class {
    func manager(_ manager: SearchTableManager, model: Photo)
    func manager(_ manager: SearchTableManager, didSelect indexPath: IndexPath)
    func manager(_ manager: SearchTableManager, remove indexPath: IndexPath?)
}

class SearchTableManager: BaseTableManager<SearchTableCell, Photo> {
    weak var delegate: SearchTableManagerDelegate?

    // MARK: - Publics -
    func configManager(_ tableView: UITableView, delegate: SearchTableManagerDelegate) {
        self.delegate = delegate
        tableView.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 1, height: 0.5))
        tableView.tableFooterView?.backgroundColor = tableView.separatorColor
        super.configManager(tableView)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.manager(self, didSelect: indexPath)
    }
    
    override func fillCell(_ cell: SearchTableCell, model: Photo, indexPath: IndexPath) {
        cell.delegate = self
        delegate?.manager(self, model: model)
    }
}

// MARK: - SearchTableCellDelegate -
extension SearchTableManager: SearchTableCellDelegate {
    func cell(remove cell: SearchTableCell) {
        delegate?.manager(self, remove: tableView.indexPath(for: cell))
    }
}
