//
//  PageViewModel.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift


protocol PageViewModelProtocol: BaseViewModelProtocol {
}

final class PageViewModel: PageViewModelProtocol {
    var photos: BehaviorRelay<[Photo]> = BehaviorRelay.init(value: [])
    var error: BehaviorRelay<NSError?> = BehaviorRelay.init(value: nil)
    
    private var photosObs: Observable<[Photo]>!
    private var disposeBag = DisposeBag()
    
    init(page: Int) {
        photosObs = Photo.photos(page: page)
    }
    
    func start() {
        photosObs.subscribe(onNext: { [weak self] (photos) in
            self?.photos.accept(photos)
        }).disposed(by: disposeBag)
        
        photosObs.catchError { [weak self]  (error) -> Observable<[Photo]> in
            self?.error.accept(error as NSError)
            return Observable.just([])
        }.subscribe().disposed(by: disposeBag)
    }
}
