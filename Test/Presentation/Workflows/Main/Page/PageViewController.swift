//
//  PageViewController.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class PageViewController: BaseViewController, CashImagesProtocol {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var page: Int = 0
    private(set) var viewModel: PageViewModel!
     
    lazy var collectionManager: PhotoCollectionManager = {
        let manager = PhotoCollectionManager()
        manager.configManager(collectionView!, delegate: self)
        return manager
    }()    

    override func configureRX() {
        viewModel = PageViewModel(page: page)
        
        viewModel.photos.asObservable().map ({ [weak self] photos in
            photos.count == 0 ? self?.showHUD() : self?.hideHUD()
            self?.collectionManager.items = photos
            self?.collectionView.reloadData()
        }).subscribe().disposed(by: disposeBag)

        viewModel.error.asObservable().skip(1).map({ [weak self] (error) in
            self?.hideHUD()
            TestError.showAlert(with: error)
        }).subscribe().disposed(by: disposeBag)
                
        viewModel.start()        
    }
}

// MARK: - PhotoCollectionManagerDelegate -
extension PageViewController: PhotoCollectionManagerDelegate {
    func manager(_ manager: PhotoCollectionManager, didSelectItem indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? PhotoCollectionCell
        var rect = self.view.convert(cell!.frame, from: collectionView)
        rect.top += 50

        if let mainViewController = self.parent?.parent?.parent as? MainViewController,
            let url = cell?.model.urls?.regular {
                setImage(url: url, imageView: mainViewController.imageViewController.imageView)
                image(url: url) { (image) in
                    mainViewController.showImageController(image: image, startRect: rect)
                }
        }
    }
}
