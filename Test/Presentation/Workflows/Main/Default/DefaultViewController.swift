//
//  DefaultViewController.swift
//  Test
//
//  Created by vaskov on 08.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class DefaultViewController: BaseViewController {
    @IBOutlet weak var pageContainerView: UIView!
    
    private var pages: [PageViewController] = []
    private var currentPage: Int = 0
    
    lazy var pageController: UIPageViewController = {
        let controller = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        controller.dataSource = self
        controller.delegate = self
        controller.view.backgroundColor = .clear
        controller.view.frame = self.view.bounds
        self.addChild(controller)
        self.view.addSubview(controller.view)
        controller.didMove(toParent: self)
        
        return controller
    }()
    
    override func viewDidLoad() {
        for i in 0...Constants.numberPages {
            let pageVC: PageViewController = Storyboards.main.viewController()
            pageVC.page = i
            pageVC.view.backgroundColor = UIColor.randomColor()
            pages.append(pageVC)
        }
        pageController.setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
    }
}

// MARK: - UIPageViewControllerDataSource, UIPageViewControllerDelegate -
extension DefaultViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? PageViewController, currentVC.page != 0 else {
            return nil
        }
        
        return pages[currentVC.page - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? PageViewController, currentVC.page < pages.count - 1 else {
            return nil
        }
        
        return pages[currentVC.page + 1]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return self.currentPage
    }
}

