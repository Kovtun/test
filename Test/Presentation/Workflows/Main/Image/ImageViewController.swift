//  
//  ImageViewController.swift
//  Test
//
//  Created by vaskov on 09.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit

final class ImageViewController: BaseViewController {
    @IBOutlet weak var imageView: UIImageView?
    
    var defaultRect: CGRect!
    weak var mainViewController: MainViewController!
    
    var image: UIImage! {
        didSet {
            imageView?.image = image
        }
    }
    
    @IBAction func tap() {
        mainViewController.hideImageController(to: defaultRect)
    }
}
