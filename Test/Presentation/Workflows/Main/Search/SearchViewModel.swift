//
//  SearchViewModel.swift
//  Test
//
//  Created by vaskov on 08.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import Api


protocol SearchViewModelProtocol: BaseViewModelProtocol {
}


final class SearchViewModel: SearchViewModelProtocol, PaginatedResponseDelegate {
    var photos: BehaviorRelay<[Photo]> = BehaviorRelay.init(value: [])
    var isSearching: BehaviorRelay<Bool> = BehaviorRelay.init(value: false)
    var error: BehaviorRelay<NSError?> = BehaviorRelay.init(value: nil)
    var paginatedResponse: PaginatedResponse<SearchParams, Photo>?
    
    private var disposeBag = DisposeBag()
    
    init(searchText: Driver<String?>) {
        searchText.map({ [weak self] (text) -> Bool in
            guard let text = text else {
                self?.error = BehaviorRelay(value: TestError.default as NSError)
                return false
            }
            self?.paginatedResponse = Photo.searchPhotos(text: text)
            return text.count >= Constants.numberCharactersNeed
        }).asObservable()
            .throttle(.milliseconds(400), scheduler: MainScheduler.instance)
            .map { [weak self] (isSearching) in
                if isSearching {
                    self?.paginatedResponse?.delegate = self
                    self?.paginatedResponse?.startReload()
                }
                self?.isSearching.accept(isSearching)
            }.subscribe().disposed(by: disposeBag)
    }
    
    func next() {
        paginatedResponse?.startNextPage()
    }
    
    // MARK: - PaginatedResponseDelegate -
    func loadBegan(_ response: AnyObject) {
    }
    
    func loadFinished(_ response: AnyObject) {
        photos.accept(paginatedResponse!.items)
    }
    
    func loadErrored(_ response: AnyObject, error: NSError) {
        self.error = BehaviorRelay.init(value: error)
        self.isSearching.accept(false)
    }
}
