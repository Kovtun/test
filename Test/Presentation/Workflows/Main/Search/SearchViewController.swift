//
//  SearchViewController.swift
//  Test
//
//  Created by vaskov on 08.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchViewController: BaseViewController, CashImagesProtocol {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var label: UILabel!
    
    private(set) var viewModel: SearchViewModel!
    
    lazy var tableManager: SearchTableManager = {
        let manager = SearchTableManager()
        manager.configManager(tableView, delegate: self)
        return manager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
    }

    // MARK: - BaseViewController -
    override func configureRX() {
        viewModel = SearchViewModel(searchText: searchBar.rx.text.asDriver())
        
        viewModel.photos.asObservable().map ({ [weak self] photos in
            self?.tableManager.items = photos
            self?.tableView.reloadData()
            self?.label.text = photos.count > 0 ? "" : "Photos are absent"
        }).subscribe().disposed(by: disposeBag)
        
        viewModel.error.asObservable().map({ (error) in
            TestError.showAlert(with: error)
        }).subscribe().disposed(by: disposeBag)
        
        viewModel.isSearching.asObservable().map({ [weak self] (isSearching) in
            self?.label.text = isSearching ? "" : "Enter 3 symbols or more..."
        }).subscribe().disposed(by: disposeBag)
    }
}

// MARK: - UISearchBarDelegate -
extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}

// MARK: - SearchTableManagerDelegate -
extension SearchViewController: SearchTableManagerDelegate {
    func manager(_ manager: SearchTableManager, remove indexPath: IndexPath?) {
        if let response = viewModel.paginatedResponse, let row = indexPath?.row {
            response.items.remove(at: row)
            tableManager.items.remove(at: row)
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        }
    }
    
    func manager(_ manager: SearchTableManager, didSelect indexPath: IndexPath) {
        let rect = tableView.rectForRow(at: indexPath)
        var rect1 = self.view.convert(rect, from: tableView)
        rect1.top += 60
        rect1.height -= 20
        
        if let mainViewController = self.parent as? MainViewController,
            let response = viewModel.paginatedResponse,
            let url = response.items[indexPath.row].urls?.regular {
                setImage(url: url, imageView: mainViewController.imageViewController.imageView)
                image(url: url) { (image) in
                    mainViewController.showImageController(image: image, startRect: rect1)
                }
        }        
    }
    
    func manager(_ manager: SearchTableManager, model: Photo) {
        if let response = viewModel.paginatedResponse, response.hasMoreItems, model.id == nil {
            let indexPath = IndexPath.init(row: response.items.count - 1, section: 0)
            if let isContain = tableView.indexPathsForVisibleRows?.contains(indexPath), isContain {
                response.hasMoreItems = false
                response.startNextPage()
            }
        }
    }
}




