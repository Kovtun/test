//
//  MainViewController.swift
//  Test
//
//  Created by vaskov on 2/18/19.
//  Copyright (c) 2019 Home. All rights reserved.
//
//

import UIKit
import RxCocoa
import RxSwift


final class MainViewController: BaseViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var defaultContainerView: UIView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var imageContainerView: UIView!
    
    var imageViewController: ImageViewController {
        return self.children.last as! ImageViewController
    }

    // MARK: - BaseViewController -
    override func configureRX() {
        segmentedControl!.rx.value.changed.bind { [weak self] (activeSegmentedTab) in
            self?.searchContainerView.isHidden = activeSegmentedTab == 1
            self?.defaultContainerView.isHidden = activeSegmentedTab != 1
        }.disposed(by: disposeBag)
    }
    
    func showImageController(image: UIImage?, startRect: CGRect) {
        guard let image = image else {
            return
        }
        self.view.endEditing(true)
        
        imageContainerView.frame = startRect
        imageContainerView.isHidden = false
        imageContainerView.backgroundColor = UIColor.white
        imageViewController.image = image
        imageViewController.mainViewController = self
        imageViewController.defaultRect = startRect
        
        UIView.animate(withDuration: Constants.animationDuration, delay: 0.1, options: .beginFromCurrentState, animations: {
            self.imageContainerView.frame = self.view.frame
            self.imageContainerView.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideImageController(to rect: CGRect) {
        UIView.animate(withDuration: Constants.animationDuration, animations: {
            self.imageContainerView.frame = rect
            self.imageContainerView.layoutIfNeeded()
        }) { bool in
            self.imageContainerView.isHidden = true
        }
    }
}
