//
//  Constants.swift
//
//  Created by vaskov on 19.09.18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit


struct Constants {
    static let animationDuration     = 0.3
    static let kApiArray             = "Array"
    static let clientId              = "4c9fbfbbd92c17a2e95081cec370b4511659666240eb4db9416c40c641ee843b"
    static let numberRows            = 3
    static let numberPages           = 10
    static let numberPicturesPerPage = 30
    static let numberCharactersNeed  = 3
}

