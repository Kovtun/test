//
//  Storyboards.swift
//
//  Created by vaskov on 19.04.18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import UIKit


enum Storyboards: String {
    case main = "Main"
    
    var storyboard: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
    
    func viewController(_ identifier: String) -> UIViewController {
        let controller = self.storyboard.instantiateViewController(withIdentifier: identifier)
        return controller 
    }
    
    func baseViewController(_ identifier: String) -> BaseViewController {
        let controller = self.storyboard.instantiateViewController(withIdentifier: identifier)
        return controller as! BaseViewController
    }
    
    func viewController<T: UIViewController>() -> T {
        let controller = self.storyboard.instantiateViewController(withIdentifier: String(describing: T.self))
        
        guard let vc = controller as? T else {
            assert(true)
            return T()
        }
        return vc
    }
}
