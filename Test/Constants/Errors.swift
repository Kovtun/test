//
//  Errors.swift
//  Test
//
//  Created by vaskov on 17.08.18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit
import Alamofire


enum TestError: Error, Equatable {
    case `default`
    case paginatedError
    
    static func showAlert(with error: Error?, title: String = "Error") {
        guard error != nil else {
            return
        }
        if let text = TestError.text(with: error!), !text.isEmpty {
            AlertHelper.showMessage(message: AlertMessage(title: title, message: text))
        } 
    }
    
    static func text(with error: Error) -> String? {
        if let text = error is TestError ? (error as! TestError).text() : error.localizedDescription {
            return text.isEmpty ? nil : text
        }
        return nil
    }
    
    func text() -> String? {
        let text = textOfAlert()
        return text.isEmpty ? nil : text
    }
}

// MARK: - Private -
extension TestError {
    fileprivate func textOfAlert() -> String {
        switch self {
        case .default:
            return "Server error"
        case .paginatedError:
            return "Paginated Error"
        }
    }
}

// MARK: - Equatable -
func ==(lhs: Error, rhs: Error) -> Bool {
    return lhs._code == rhs._code && lhs._domain == rhs._domain
}

func ==(lhs: TestError, rhs: TestError) -> Bool {
    switch (lhs, rhs) {
    case (.`default`, .`default`):
        return true
    case (.paginatedError, .paginatedError):
        return true
    default:
        return false
    }
}

func ==(lhs: TestError, rhs: Error) -> Bool {
    return false
}

func ==(lhs: Error, rhs: TestError) -> Bool {
    return false
}


func !=(lhs: TestError, rhs: TestError) -> Bool {
    return !(lhs == rhs)
}

func !=(lhs: Error, rhs: Error) -> Bool {
    return !(lhs == rhs)
}

func !=(lhs: TestError, rhs: Error) -> Bool {
    return !(lhs == rhs)
}

func !=(lhs: Error, rhs: TestError) -> Bool {
    return !(lhs == rhs)
}

