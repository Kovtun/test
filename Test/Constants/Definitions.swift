//
//  Definitions.swift
//  Test
//
//  Created by vaskov on 25.07.17.
//  Copyright © 2017 Home. All rights reserved.
//

import Foundation
import UIKit


let APP_NAME: String = BUNDLE.infoDictionary!["CFBundleDisplayName"] as! String
let APP_BUILD: String = BUNDLE.infoDictionary!["CFBundleVersion"] as! String
let APP_VERSION: String = BUNDLE.infoDictionary!["CFBundleShortVersionString"] as! String
let TARGET_NAME: String = BUNDLE.infoDictionary!["CFBundleName"] as! String


public let NOTIFICATION_CENTER = NotificationCenter.default
public let APPLICATION         = UIApplication.shared
public let USER_DEFAULTS       = UserDefaults.standard
public let DEVICE              = UIDevice.current
public let FILE_MANAGER        = FileManager.default
public let APP_DELEGATE        = APPLICATION.delegate
public let BUNDLE              = Bundle.main
public let SCREEN              = UIScreen.main
public let SCREEN_WIDTH        = SCREEN.bounds.width
public let SCREEN_HEIGHT       = SCREEN.bounds.height
public let SYSTEM_VERSION      = (DEVICE.systemVersion as NSString).floatValue
public let DEVICE_TYPE         = DEVICE.model as NSString
public let IS_IPAD             = (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)

public let IS_IPHONE5         = !IS_IPAD && SCREEN_HEIGHT == 568
public let IS_IPHONE6_7_8     = !IS_IPAD && SCREEN_HEIGHT == 667
public let IS_IPHONE6_7_8PLUS = !IS_IPAD && SCREEN_HEIGHT == 736
public let IS_IPHONE_X_XS     = !IS_IPAD && SCREEN_HEIGHT == 812
public let IS_IPHONE_XR_XSMAX = !IS_IPAD && SCREEN_HEIGHT == 896
public let IS_IPHONE_X        = !IS_IPAD && SCREEN_HEIGHT >= 812

public let IS_IOS11         = SYSTEM_VERSION == 11
public let IS_IOS12         = SYSTEM_VERSION == 12
public let IS_IOS11_OR_MORE = SYSTEM_VERSION >= 11

public func LOCAL(_ str: String) -> String { return NSLocalizedString(str, comment: "") }
public func IMAGE(_ named: String) -> UIImage { return UIImage(named: named)! }
public func FILE_IMAGE(_ named: String) -> UIImage? {
  let path = Bundle.main.path(forResource: named, ofType: "png") ?? ""
  return UIImage(contentsOfFile: path)
}


public func degreesToRadians(_ degrees: CGFloat) -> CGFloat { return CGFloat(Double.pi) * degrees / 180.0 }
public func radiansToDegrees(_ radians: CGFloat) -> CGFloat { return radians * 180.0 / CGFloat(Double.pi) }


