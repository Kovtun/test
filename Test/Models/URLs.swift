//
//  URLs.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import Foundation
import ObjectMapper
import Api

class URLs: BaseModel {
    var full: String?
    var raw: String?
    var regular: String?
    var small: String?
    var thumb: String?
    
    override func mapping(map: Map) {
        full <- map["full"]
        raw <- map["raw"]
        regular <- map["regular"]
        small <- map["small"]
        thumb <- map["thumb"]
    }
}
