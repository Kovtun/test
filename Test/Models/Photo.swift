//
//  Photo.swift
//  Test
//
//  Created by vaskov on 08.01.2020.
//  Copyright © 2020 Home. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import Api


class Photo: BaseModel {
    var id: String?
    var urls: URLs?
    
    // MARK:- BaseModel -
    override func mapping(map: Map) {
        id <- map["id"]
        urls <- map["urls"]
    }
    
    // MARK:- Public -
    class func photos(page: Int) -> Observable<[Photo]> {
        let body = DefaultParams(page: page, perPage: Constants.numberPicturesPerPage)
        return ApiService.get(Endpoint.photos.link(Constants.clientId), body: body, responseType: [Photo.self])
    }
    
    class func allPhotos() -> PaginatedResponse<DefaultParams, Photo> {
        return PaginatedResponse(Endpoint.photos.link(Constants.clientId), request: DefaultParams()) 
    }
    
    class func searchPhotos(text: String) -> PaginatedResponse<SearchParams, Photo> {
        return PaginatedResponse(Endpoint.search.link(Constants.clientId), request: SearchParams(query: text))
    }
}
