//
//  DefaultParams.swift
//  Test
//
//  Created by vaskov on 10.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import Api


class DefaultParams: BaseModel, PaginatedRequest {
    var page: Int = 1
    var perPage: Int = 10
    var orderBy: String = "latest"

    init(page: Int = 1, perPage: Int = 10, orderBy: String = "latest") {
        self.page = page
        self.perPage = perPage
        self.orderBy = orderBy
        super.init()
    }
    
    override func mapping(map: Map) {
        page <- map["page"]
        perPage <- map["per_page"]
        orderBy <- map["order_by"]
    }
    
    required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
}

