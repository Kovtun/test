//
//  SearchParams.swift
//  Test
//
//  Created by vaskov on 08.01.2020.
//  Copyright © 2020 vaskov. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import Api


class SearchParams: BaseModel, PaginatedRequest {    
    var page: Int = 1
    var perPage: Int = 1
    var query: String = "_"
    var collections: String?
    var orientation: String = "portrait"
    
    init(query: String, page: Int = 1, perPage: Int = 10, collections: [String] = [""], orientation: String = "portrait") {
        self.query = query
        self.page = page
        self.perPage = perPage
        self.orientation = orientation
        
        let items = collections.compactMap({ (str) -> String? in
            return str.trimmingCharacters(in: CharacterSet.init(charactersIn: " \n")) == "" ? nil : str
        })
        self.collections = items.count == 0 ? nil : items.joined(separator: ",")
        super.init()
    }
    
    required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        page <- map["page"]
        perPage <- map["per_page"]
        query <- map["query"]
        collections <- map["collections"]
        orientation <- map["orientation"]
    }
}
