//
//  JXBaseModel.swift
//  Cosmunity
//
//  Created by vaskov on 12.09.18.
//  Copyright © 2018 Home Inc. All rights reserved.
//

import ObjectMapper
import AlamofireObjectMapper


open class BaseModel: NSObject, Mappable {
    open func mapping(map: Map) {
    }

    public override init() {
        super.init()
    }
    
    required public init?(map: Map) {
    }
}


