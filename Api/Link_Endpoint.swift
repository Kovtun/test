//
//  ApiBittiq.swift
//  Bittiq
//
//  Created by vaskov on 07/04/2019.
//  Copyright © 2019 Home. All rights reserved.
//

import Foundation


open class Link {
    #if DEBUG
        static let baseUrl = "https://api.unsplash.com/"
    #endif
    
    #if RELEASE
        static let baseUrl = "https://api.unsplash.com/"
    #endif
    
    private(set) var link: String
    private(set) var endpoint: String
    
    init(_ endpoint: String) {
        self.endpoint = endpoint
        link = String(format: "%@%@", Link.baseUrl, endpoint)
    }
}

public enum Endpoint: String {
    case `default` = "%@"
    case search = "search/photos/?client_id=%@"
    case photos = "photos/?client_id=%@"
    
    public func link(_ elements: Any...) -> Link {
        return Link(self.endpoint(elements))
    }
    
    func endpoint(_ elements: Any...) -> String {
        let link = String(format: self.rawValue, arguments: elements as! [CVarArg])
        
        let _link = link.components(separatedBy: "=").map {
             $0.trimmingCharacters(in: CharacterSet(charactersIn: "() \n"))
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }.joined(separator: "=") as NSString
        
        return _link.substring(to: 1) == "/" ? _link.substring(from: 1) : _link as String
    }
}

