//
//  ApiService.swift
//  RX
//
//  Created by vaskov on 03.10.2019.
//  Copyright © 2019 vaskov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Alamofire
import ObjectMapper


protocol ApiProtocol {
    static func get   <T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding, responseType: T.Type) -> Observable<T>
    static func get   <T: Mappable>(_ link: Link, body: AnyObject, headers: [String: String]?, encode: ParameterEncoding, responseType: T.Type) -> Observable<T>
    static func post  <T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding, responseType: T.Type) -> Observable<T>
    static func patch <T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding, responseType: T.Type) -> Observable<T>
    static func put   <T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding, responseType: T.Type) -> Observable<T>
    static func delete<T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding, responseType: T.Type) -> Observable<T>
    
    static func get <T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding,  responseType: [T.Type]) -> Observable<[T]>
    static func post<T: Mappable>(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding,  responseType: [T.Type]) -> Observable<[T]>
    
    static func get   (_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding) -> Observable<NSDictionary>
    static func post  (_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding) -> Observable<NSDictionary>
    static func patch (_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding) -> Observable<NSDictionary>
    static func put   (_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding) -> Observable<NSDictionary>
    static func delete(_ link: Link, body: AnyObject?, headers: [String: String]?, encode: ParameterEncoding) -> Observable<NSDictionary>
}


public class ApiService: ApiProtocol {
    static private let shared = ApiService()

    lazy private var session: Session = {
        return Session.default
    }()
}

extension ApiService {
    public class func get<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = URLEncoding.default,  responseType: T.Type) -> Observable<T> {
        return self._request(Alamofire.HTTPMethod.get, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: T.self)
    }
    
    public class func get<T: Mappable>(_ link: Link, body: AnyObject, headers: [String: String]? = nil, encode: ParameterEncoding = URLEncoding.default,  responseType: T.Type) -> Observable<T> {
        return self._request(Alamofire.HTTPMethod.get, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: T.self)
    }
    
    public class func post<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default,  responseType: T.Type) -> Observable<T> {
        return self._request(Alamofire.HTTPMethod.post, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: T.self)
    }
    
    public class func patch<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default,  responseType: T.Type) -> Observable<T> {
        return self._request(Alamofire.HTTPMethod.patch, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: T.self)
    }
    
    public class func put<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default,  responseType: T.Type) -> Observable<T> {
        return self._request(Alamofire.HTTPMethod.put, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: T.self)
    }
    
    public class func delete<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default,  responseType: T.Type) -> Observable<T> {
        return self._request(Alamofire.HTTPMethod.delete, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: T.self)
    }
}
    
extension ApiService {
    public class func get<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = URLEncoding.default,  responseType: [T.Type]) -> Observable<[T]> {
        return self._request(Alamofire.HTTPMethod.get, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: [T.self])
    }
    
    public class func post<T: Mappable>(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default,  responseType: [T.Type]) -> Observable<[T]> {
        return self._request(Alamofire.HTTPMethod.post, endpoint: link.link, parameters: body, encode: encode, headers: headers, responseType: [T.self])
    }
}
    
extension ApiService {
    public class func get(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = URLEncoding.default) -> Observable<NSDictionary> {
        return self._request(Alamofire.HTTPMethod.get, endpoint: link.link, parameters: body, encode: encode, headers: headers)
    }
    
    public class func post(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default) -> Observable<NSDictionary> {
        return self._request(Alamofire.HTTPMethod.post, endpoint: link.link, parameters: body, encode: encode, headers: headers)
    }
    
    public class func patch(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default) -> Observable<NSDictionary> {
        return self._request(Alamofire.HTTPMethod.patch, endpoint: link.link, parameters: body, encode: encode, headers: headers)
    }
    
    public class func put(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default) -> Observable<NSDictionary> {
        return self._request(Alamofire.HTTPMethod.put, endpoint: link.link, parameters: body, encode: encode, headers: headers)
    }
    
    public class func delete(_ link: Link, body: AnyObject? = nil, headers: [String: String]? = nil, encode: ParameterEncoding = JSONEncoding.default) -> Observable<NSDictionary> {
        return self._request(Alamofire.HTTPMethod.delete, endpoint: link.link, parameters: body, encode: encode, headers: headers)
    }
}

extension ApiService {
    private class func _request<T: Mappable>(_ method: Alamofire.HTTPMethod,
                        endpoint: String,
                        parameters: AnyObject? = nil,
                        encode: ParameterEncoding,
                        headers: [String: String]? = nil,
                        responseType: T.Type) -> Observable<T> {
        return self._request(method,
                             endpoint: endpoint,
                             parameters: parameters,
                             encode: encode,
                             headers: headers).map { body -> T in
                                return Mapper<T>().map(JSONObject: body)!
        }
    }

    private class func _request<T: Mappable>(_ method: Alamofire.HTTPMethod,
                                                 endpoint: String,
                                                 parameters: AnyObject? = nil,
                                                 encode: ParameterEncoding,
                                                 headers: [String: String]? = nil,
                                                 responseType: [T.Type]) -> Observable<[T]> {
        return self._request(method,
                             endpoint: endpoint,
                             parameters: parameters,
                             encode: encode,
                             headers: headers).map { body -> [T] in
                                return Mapper<T>().mapArray(JSONArray: body["Array"] as! [[String : Any]])
        }
    }
    
    private class func _request(_ method: Alamofire.HTTPMethod,
                        endpoint: String,
                        parameters: AnyObject? = nil,
                        encode: ParameterEncoding,
                        headers: [String: String]? = nil) -> Observable<NSDictionary> {
        return self.shared.request(method,
                                   endpoint: endpoint,
                                   parameters: parameters,
                                   encode: encode,
                                   headers: headers)
    }
    
    private func request(_ method: Alamofire.HTTPMethod,
                 endpoint: String,
                 parameters: AnyObject? = nil,
                 encode: ParameterEncoding,
                 headers: [String: String]? = nil) -> Observable<NSDictionary> {
        
        // Map the parameters to a dictionary
        let safeParameters = parameters ?? [:] as AnyObject
        let _parameters = safeParameters is BaseModel ? Mapper().toJSON(safeParameters as! BaseModel) : (safeParameters as? [String: Any])!
        let _headers = HTTPHeaders((headers == nil ? [:] : headers!))
        
        print("API+ BEFORE", endpoint, _parameters)
        
        return Observable.create { observer -> Disposable in
            self.session.request(endpoint, method: method, parameters: _parameters, encoding: encode, headers: _headers)
                .validate()
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success(let dict):
                        print("API+ AFTER ACCESS", response.request?.url as Any)
                        print(dict)

                        if let newDict = dict as? NSDictionary {
                            observer.onNext(newDict)
                        } else if let newArray = dict as? NSArray {
                            observer.onNext(["Array" : newArray])
                        } else {
                            let finalDict: NSDictionary = [:]
                            observer.onNext(finalDict)
                        }
                        observer.onCompleted()
                    case .failure(let error):
                        print("API+ AFTER FAIL", response.request?.url as Any, error.localizedDescription)
                        return observer.onError(error)
                    }
                })
            return Disposables.create()
        }.distinctUntilChanged()
    }
}
