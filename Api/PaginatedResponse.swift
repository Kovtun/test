//
//  PaginatedResponse.swift
//  Cosmunity
//
//  Created by vaskov on 3/14/17.
//  Copyright © 2017 Home. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import RxSwift
import RxCocoa


public protocol PaginatedResponseDelegate: class {
    func loadBegan(_ response: AnyObject)
    func loadFinished(_ response: AnyObject)
    func loadErrored(_ response: AnyObject, error: NSError)
}


public protocol AnonymousPaginatedResponse: class {
    func startReload()
    func startNextPage()
}

public protocol PaginatedRequest: Mappable {
    var page: Int {set get}
    var perPage: Int {set get}
}


open class PaginatedResponse<Request: PaginatedRequest, Response: Mappable>: BaseModel, AnonymousPaginatedResponse {
    open var total: Int = 0
    open var totalPages: Int = 0
    open var items: [Response] = []
    open var hasMoreItems: Bool = true
    
    open weak var delegate: PaginatedResponseDelegate?
    open private(set) var isLoading: Bool = false
    open private(set) var link: Link?
    
    private let bag = DisposeBag()
    private var defaultRequest: Request!
    private var request: Request!
    private var currentTask: Observable<PaginatedResponse<Request, Response>>?

    //MARK: - Initialization -
    public init(_ link: Link, request: Request) {
        self.link = link
        self.defaultRequest = request
        self.request = request
        
        super.init()
        reset()
    }

    required public init?(map: Map) {
        super.init(map: map)
        mapping(map: map)
    }

    //MARK: - BaseModel -
    override open func mapping(map: Map) {
        total <- map["total"]
        totalPages <- map["total_pages"]
        items <- map["results"]
        items <- map["Array"]
    }

    //MARK: - Publics -
    func cancel() {
        self.currentTask = nil
    }

    func reset() {
        self.currentTask = nil
        self.items = []
        self.request = self.defaultRequest
        self.hasMoreItems = true
    }

    func reload() -> Observable<PaginatedResponse<Request, Response>> {
        reset()
        return task(isNext: false)
    }

    func nextPage() -> Observable<PaginatedResponse<Request, Response>> {
        request.page += 1
        return task(isNext: true)
    }

    // MARK: AnonymousPaginatedResponse
    public func startReload() {
        reload().subscribe().disposed(by: bag)
    }

    public func startNextPage() {
        nextPage().subscribe().disposed(by: bag)
    }
}

// MARK: - Privates -
extension PaginatedResponse {
    private func task(isNext: Bool) -> Observable<PaginatedResponse<Request, Response>> {
        guard currentTask == nil else {
            return currentTask!
        }

        currentTask = guardTask(isNext: isNext)
        self.delegate?.loadBegan(self)
        self.isLoading = true

        return self.currentTask!
    }

    private func guardTask(isNext: Bool) -> Observable<PaginatedResponse<Request, Response>> {
        return ApiService.get(link!, body: request as AnyObject, responseType: (PaginatedResponse<Request, Response>).self).map { [weak self] (response) -> PaginatedResponse<Request, Response> in
            self?.isLoading = false
            if response.totalPages > 0 {
                self?.hasMoreItems = (self?.request.page ?? Int.max) < response.totalPages
            } else {
                self?.hasMoreItems = response.items.count == self?.request.perPage
            }
            
            if isNext {
                self?.items.insert(contentsOf: response.items, at: (self?.items.count ?? 1) - 1)
                if let hasMoreItems = self?.hasMoreItems, !hasMoreItems {
                    self?.items.remove(at: (self?.items.count ?? 1) - 1)
                }
            } else {
                self?.items = response.items
                if let hasMoreItems = self?.hasMoreItems, hasMoreItems {
                    self?.items.append(Response.init(map: Map(mappingType: MappingType.fromJSON, JSON: ["":""]))!)
                }
            }

            if self != nil {
                self!.delegate?.loadFinished(self!)
            }
            
            self?.currentTask = nil
            return response
        }.catchError { [weak self] (error) -> Observable<PaginatedResponse<Request, Response>> in
            if self != nil {
                self!.delegate?.loadErrored(self!, error: error as NSError)
            }
            self?.isLoading = false
            self?.currentTask = nil
            self?.hasMoreItems = false
            return Observable.error(error)
        }
    }
}
